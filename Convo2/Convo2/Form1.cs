﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class FrmMain : Form
    {

        private DataTable dtExtinguidores;
        public FrmMain()
        {
            InitializeComponent();
        }

        private void gestionarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionExtinguidores ge = new GestionExtinguidores();
            ge.MdiParent = this;
            ge.DsExtinguidores = dsExtinguidores;
            ge.Show();

        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtExtinguidores = dsExtinguidores.Tables["Extinguidores"];
        }
    }
}
