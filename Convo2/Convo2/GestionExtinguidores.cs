﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class GestionExtinguidores : Form
    {
        private DataSet dsExtinguidores;
        private BindingSource bsExtinguidores;

        public DataSet DsExtinguidores
        {
            get
            {
                return dsExtinguidores;
            }

            set
            {
                dsExtinguidores = value;
            }
        }

        public GestionExtinguidores()
        {
            InitializeComponent();
            bsExtinguidores = new BindingSource();
        }

        private void GestionExtinguidores_Load(object sender, EventArgs e)
        {
            bsExtinguidores.DataSource = dsExtinguidores;
            bsExtinguidores.DataMember = dsExtinguidores.Tables["Extinguidores"].TableName;
            dataGridView1.DataSource = bsExtinguidores;
            dataGridView1.AutoGenerateColumns = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Extinguidor ex = new Extinguidor();
            ex.DtExtinguidor = DsExtinguidores.Tables["Extinguidores"];
            ex.DsExtinguidor = DsExtinguidores;
            ex.ShowDialog();
        }
    }
}
