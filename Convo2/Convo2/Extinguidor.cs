﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class Extinguidor : Form
    {
        private DataTable dtExtinguidor;
        private DataSet dsExtinguidor;
        private DataRow drExtinguidor;
        private BindingSource bsExtiguidor;

        public DataTable DtExtinguidor
        {
            get
            {
                return dtExtinguidor;
            }

            set
            {
                dtExtinguidor = value;
            }
        }

        public DataSet DsExtinguidor
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }

        public DataRow DrExtinguidor
        {
            set
            {
                drExtinguidor = value;
                cmbCategoria.SelectedValue = drExtinguidor["Categoria"].ToString();
                txtMarca.Text = drExtinguidor["Marca"].ToString();
                txtCapacidad.Text = drExtinguidor["Capacidad"].ToString();
                cmbTipo.SelectedValue = drExtinguidor["Tipo_extinguidor"].ToString();
                txtUnidad.Text = drExtinguidor["Unidad_medida"].ToString();
                txtCantidad.Text = drExtinguidor["Cantidad"].ToString();
            }
        }

        public Extinguidor()
        {
            InitializeComponent();
            bsExtiguidor = new BindingSource();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string categoria, marca, tipo, unidad,com;
            int capacidad=0, cantidad=0;

            if (cmbCategoria.SelectedIndex == 0)
            {
                categoria = "Americano";
            }
            else
            {
                categoria = "Europeo";
            }
            marca = txtMarca.Text;
            if(cmbTipo.SelectedIndex==0){
                tipo = "CO2";
            }
            else
            {
                if (cmbTipo.SelectedIndex == 1)
                {
                    tipo = "H2O";
                }
                else
                {
                    tipo = "OS";
                }
            }
            unidad = txtUnidad.Text;
            string[] path = { unidad, lblMedida.Text };
            com = Path.Combine(path);
            try {
                capacidad = Int32.Parse(txtCapacidad.Text);
                cantidad = Int32.Parse(txtCantidad.Text);
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Error recibir datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

            if(drExtinguidor != null)
            {
                DataRow row = dtExtinguidor.NewRow();
                int index = dtExtinguidor.Rows.IndexOf(drExtinguidor);
                row["Id"] = drExtinguidor["Id"];
                row["Categoria"] = categoria;
                row["Marca"] = marca;
                row["Capacidad"] = capacidad;
                row["Tipo_extinguidor"] = tipo;
                row["Unidad_medida"] = com;
                row["Cantidad"] = cantidad;

                dtExtinguidor.Rows.RemoveAt(index);
                dtExtinguidor.Rows.InsertAt(row, index);
            }
            else
            {
                dtExtinguidor.Rows.Add(dtExtinguidor.Rows.Count + 1, categoria, marca, capacidad, tipo, com, cantidad);
            }
            Dispose();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTipo.SelectedIndex == 0)
            {
                lblMedida.Text = "Libras";
            }
            else
            {
                if (cmbTipo.SelectedIndex == 1)
                {
                   lblMedida.Text = "Litros";
                }
                else
                {
                    lblMedida.Text = "Libras";
                }
            }
        }

        private void Extinguidor_Load(object sender, EventArgs e)
        {
            bsExtiguidor.DataSource = DsExtinguidor;
            bsExtiguidor.DataMember = DsExtinguidor.Tables["Extinguidores"].TableName;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
